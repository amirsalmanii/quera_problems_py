client_in = [input('') for i in range(5)]
answers = []

def FindMolana(val, client_in, value_split):
    length_of_list = len(value_split) - 1
    f_index = value_split.index('M')
    end_index = f_index + 6
    result = value_split[f_index:end_index]
    result = ''.join(result)
    if result == 'MOLANA':
        answers.append(client_in.index(val) + 1)


def FindHafez(val, client_in, value_split):
    length_of_list = len(value_split) - 1
    f_index = value_split.index('H')
    end_index = f_index + 5
    result = value_split[f_index:end_index]
    result = ''.join(result)
    if result == 'HAFEZ':
        answers.append(client_in.index(val) + 1)


for val in client_in:
    value_split = list(val)
    if 'M' in value_split:
        FindMolana(val, client_in, value_split)
    elif 'H' in value_split:
        FindHafez(val, client_in, value_split)

for answer in answers:
    print(answer, end=' ')



